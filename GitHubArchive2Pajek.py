"""

The script takes as its input an archive of GitHub events (e.g. see
http://www.githubarchive.org) and it represents the events in Pajek-based
format.

GitHub provides 18 event types, which range from new commits and fork events,
to opening new tickets, commenting, and adding members to a project.
The activity is aggregated in hourly archives, each of which contains a stream
of JSON encoded GitHub events.

Resulting network describes the types of "things" and relationships
between them as a multidimensional network in which the nodes represent various
event types and users.

The script saves information on vertices and edges into .net file, and the
type of the nodes into .clu file.

See also:

- GitHub Archive (http://www.githubarchive.org),
- JSON schema of event types (http://developer.github.com/v3/activity/events/
  types/#memberevent),
- A graph representation of GitHub (http://hortonworks.com/blog/big-graph-data-
  on-hortonworks-data-platform).

"""

import os
import json
import sys

t_user = 'user'
t_commit = 'commit'
t_repo = 'repository'
t_issue = 'issue'
t_comment = 'comment'
t_wikipage = 'wikipage'
t_gist = 'gist'

type2ind = {
        t_user:1,
        t_commit:2,
        t_repo:3,
        t_issue:4,
        t_comment:5,
        t_wikipage:6,
        t_gist:7
    }

def _parse_json(json_input):
    events = []
    for line in open(json_input):
        events.append(json.loads(line))
    return events

def _push_event(event):
    u1 = event['actor']
    u2 = event['repository']['url']
    u3 = event['payload']['head']
    e = [(u1, u3), (u3, u2)]
    n2p = {u1:t_user, u2:t_repo, u3:t_commit}
    return n2p, e

def _member_event(event):
    u1 = event['actor']
    u2 = event['repository']['url']
    u3 = event['payload']['member']['login']
    e = [(u1, u3), (u3, u2)]
    n2p = {u1:t_user, u2:t_repo, u3:t_commit}
    return n2p, e

def _follow_event(event):
    u1 = event['actor']
    u2 = event['payload']['target']['login']
    e = [(u1, u2)]
    n2p = {u1:t_user, u2:t_user}
    return n2p, e

def _gist_event(event):
    u1 = event['actor']
    u2 = event['payload']['id']
    e = [(u1, u2)]
    n2p = {u1:t_user, u2:t_gist}
    return n2p, e

def _gollum_event(event):
    u1 = event['actor']
    us = [gollum['html_url'] for gollum in event['payload']['pages']]
    e = [(u1, u) for u in us]
    n2p = {u:t_wikipage for u in us}
    n2p.update({u1:t_user})
    return n2p, e

def _issue_comment_event(event):
    u1 = event['actor']
    u2 = event['payload']['comment_id']
    u3 = event['payload']['issue_id']
    e = [(u1, u2), (u2, u3)]
    n2p = {u1:t_user, u2:t_comment, u3:t_issue}
    return n2p, e

def _pull_request_review_comment_event(event):
    u1 = event['actor']
    u2 = event['payload']['comment']['id']
    u3 = event['repository']['url']
    e = [(u1, u2), (u2, u3)]
    n2p = {u1:t_user, u2:t_comment, u3:t_repo}
    return n2p, e

def _commit_comment_event(event):
    u1 = event['actor']
    u2 = event['payload']['comment_id']
    u3 = event['repository']['url']
    e = [(u1, u2), (u2, u3)]
    n2p = {u1:t_user, u2:t_comment, u3:t_repo}
    return n2p, e

def _issues_event(event):
    u1 = event['actor']
    u2 = event['payload']['issue']
    e = [(u1, u2)]
    n2p = {u1:t_user, u2:t_issue}
    return n2p, e

def _create_event(event):
    if 'repository' not in event:
        return {}, []
    u1 = event['actor']
    u2 = event['repository']['url']
    e = [(u1, u2)]
    n2p = {u1:t_user, u2:t_repo}
    return n2p, e

def _delete_event(event):
    return _create_event(event)

def _download_event(event):
    return _create_event(event)

def _fork_event(event):
    return _create_event(event)

def _fork_apply_event(event):
    return _create_event(event)

def _pull_request_event(event):
    return _create_event(event)

def _watch_event(event):
    return _create_event(event)

def _public_event(event):
    return _create_event(event)

def main(input_path, output_dir):
    name = input_path.split(os.sep)[-1].split(".")[0]
    events = _parse_json(input_path)

    node2prop = {}
    edges = set()

    event_methods = {
        'GistEvent': _gist_event,
        'GollumEvent': _gollum_event,
        'PullRequestReviewCommentEvent': _pull_request_review_comment_event,
        'IssueCommentEvent': _issue_comment_event,
        'CommitCommentEvent': _commit_comment_event,
        'IssuesEvent': _issues_event,
        'CreateEvent': _create_event,
        'DeleteEvent': _delete_event,
        'DownloadEvent': _download_event,
        'ForkEvent': _fork_event,
        'ForkApplyEvent': _fork_apply_event,
        'PullRequestEvent': _pull_request_event,
        'WatchEvent': _watch_event,
        'PublicEvent': _public_event,
        'PushEvent': _push_event,
        'MemberEvent': _member_event,
        'FollowEvent': _follow_event
    }

    for event in events:
        if event['type'] in event_methods:
            n2p, ed = event_methods[event['type']](event)
            node2prop.update(n2p)
            for e in ed:
                if (e[1], e[0], event['type']) not in edges:
                    edges.add((e[0], e[1], event['type']))

    node2ind = {n:i+1 for i, n in enumerate(node2prop)}

    _write_net(name, output_dir, node2ind, edges)
    _write_clu(name, output_dir, node2prop, node2ind, type2ind)

def _write_net(name, output_dir, node2ind, edges):
    path = os.path.join(output_dir, "%s.net" % name)
    print "Writing: %s" % path
    print "# vertices: %d" % len(node2ind)
    print "# edges: %s" % len(edges)
    f = open(path, 'w')
    f.write("*Vertices %d\n" % len(node2ind))

    ind2node = {i:n for n, i in node2ind.iteritems()}
    for i in xrange(1, len(ind2node)+1):
        f.write(' %d "%s"\n' % (i, ind2node[i]))

    f.write("*Edges\n")
    for e1, e2, label in edges:
        f.write(' %d %d 1 l "%s"\n' % (node2ind[e1], node2ind[e2], label))
    f.close()

def _write_clu(name, output_dir, node2prop, node2ind, prop2ind):
    path = os.path.join(output_dir, "%s.clu" % name)
    print "Writing: %s" % path
    f  = open(path, 'w')
    f.write("*Vertices %d\n" % len(node2prop))

    node_props = {node2ind[node]:prop2ind[prop] for
                  node, prop in node2prop.iteritems()}

    for i in xrange(1, len(node_props)+1):
        f.write(" %d\n" % node_props[i])
    f.close()

if __name__ == '__main__':
    if len(sys.argv) < 3:
        print "Usage: python %s <path-to-github-archive-file> " \
              "<path-to-output-dir>" % sys.argv[0]
    else:
        main(sys.argv[1], sys.argv[2])