GitHubArchive2Pajek
===================
The script takes as its input an archive of GitHub events (e.g. see
http://www.githubarchive.org) and it represents the events in Pajek-based
format.

GitHub provides 18 event types, which range from new commits and fork events,
to opening new tickets, commenting, and adding members to a project.
The activity is aggregated in hourly archives, each of which contains a stream
of JSON encoded GitHub events.

Resulting network describes the types of "things" and relationships
between them as a multidimensional network in which the nodes represent various
event types and users.

The script saves information on vertices and edges into .net file, and the
type of the nodes into .clu file.

See also:

- GitHub Archive (http://www.githubarchive.org),
- JSON schema of event types (http://developer.github.com/v3/activity/events/
  types/#memberevent),
- A graph representation of GitHub (http://hortonworks.com/blog/big-graph-data-
  on-hortonworks-data-platform).